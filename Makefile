pdf: report.tex
	pdflatex report.tex

pdf-bib: report.tex refs.bib
	pdflatex report.tex
	bibtex report.aux
	pdflatex report.tex
	pdflatex report.tex

clean:
	rm -rf report.aux report.log report.pdf report.bbl report.blg report.out

