% vim: wrap linebreak nolist
\documentclass[letterpaper,twocolumn,twoside]{article}

\usepackage{fullpage}
\usepackage{hyperref}

\title{Feature Driven Development}
\author{Jacob Gross, Matthew Mahnke, Jake Robers}
\date{10 February 2016}

\begin{document}
\maketitle

Like scrum, feature driven development (FDD) is an iterative and incremental software development process.
FDD blends many best-practices, such as developing by feature, feature teams, and regular builds, into one process.
The intent of these practices are to drive a client-valued perspective, delivering working software in a timely manner.


\section{FDD Project Lifecycle}
\label{sec:lifecycle}

\subsection{Develop an Overall Model}
\label{subsec:overall-model}

The development team and any domain experts work with the Chief Architect to get a high-level walkthrough of the system and its context.
Each part of the domain is then broken into its constituent components where the domain experts provide a more detailed walkthrough.
During this walkthrough, the development team works in groups to construct object models for each component.
After having finished constructing models, their results are presented to the group for discussion and peer-review.
Of these various object models for the same domain model, the best is selected and merged into the overall model for this particular component.
This process is then repeated for all other domain components.

The overall domain model is updated with content from process IV \nameref{subsec:design-feature}.

\subsection{Build a Features List}
\label{subsec:features-list}

The Chief Programmers from the previous step decompose the functionality of the domain.
These decomposed domain areas are then broken down further into \emph{major} features sets which is further decomposed into individual activities, or feature sets.
These feature sets become your itemized features.
Finally, you should have a hierarchical list of features.

\subsection{Plan by Feature}
\label{subsec:plan-feature}

The Project Manager, Development Manager, and Chief Programmers determine the order that features will be implemented based on complexity, load on the developers, and the feature's dependencies.
Typically, they determine in which order the features will be developed, assign feature sets to Chief Programmers, then assign individual classes to developers.

The tasks in this process are not in a strict order, and are often refined to suit those doing the work.

\subsection{Design by Feature}
\label{subsec:design-feature}

Each Chief Programmer selects features for development from those assigned to him/her.
Often times, the Chief Programmer will schedule a small groups of features to be implemented at a time.
They're likely to choose multiple features that that use the same class, so that the same developers are also on the team.
(This group of features is called a Chief Programmer work package.)
Based on the features, a team is formed identifying the owners of classes that are involved in the development in of the selected features.
The Chief Programmer grooms the object model based on sequence diagrams.
The design is then verified.

\subsection{Build by Feature}
\label{build-feature}

Working on the work package produced in the previous step, the class owners implement the their classes to support the features.
The code is then unit tested and inspected.
After being successfully test and inspected, it it built.


\section{Software Requirements}
\label{sec:requirements}

While gathering requirements for a project, FDD makes a clear distinction between various types of requirements, such as product, system, and non-functional.
Also, FDD has a clearly defined process for defining all actors and management models, analyzing requirements for conflicts, and validating the requirements to be implemented.
However while eliciting requirements, the gathering and managing requirements are not specified in FDD; the Chief Architect and Chief Programmers gather the high-level description for the system from domain experts.


\section{Software Construction}
\label{sec:construction}

FDD minimizes code complexity by breaking down a large problem into smaller problems until they are easily managed.
Once each smaller problem has been solved, they are easily integrated into each other.
Because it promotes CI, refactoring and collective code ownership, FDD quickly adapts to changes in requirements.
This process can quickly detect bugs and defects because pair programming and unit testing are strongly encouraged.


\section{The People}
\label{sec:people}

FDD defines six key people; they are the:
\begin{description}
  \item[Project Manager] who administrates the project, is responsible for managing budgets, managing equipment, space, and resources, etc.
  \item[Chief Architect] is responsible of overall design of the system.
    They run workshop design sessions; they steer the project around any obstacles.
    The CA must have excellent technical and modeling skills.
  \item[Development Manager] leads day-to-day development. They assist the Chief Programmers when they cannot resolve everyday conflicts.
  \item[Chief Programmers] are experienced developers who participate in the high-level requirements analysis.
    They lead teams of 3--6 developers to design and develop new features.
  \item[Class Owners] are developers who work on the Chief Programmer to design, code, test, and document a specific feature.
  \item[Domain Expert(s)] is (are) any one who has knowledge that the developers rely on in order to deliver the correct system.
\end{description}

There are also several support roles that assist the former people:
\begin{description}
  \item[Release Manager] reports directly to the Project Manager and ensures the Chief Programmers report progress each week.
  \item[Language Guru] They are masters of the  programming language that is being used on the project.
    They are especially important when working on a project where the language is now to most of the developers.
  \item[Build Engineer] They setup, maintain, and run the build process.
  \item[Toolsmith] creates small tools for helping the various teams.
  \item[System Administrator] configures, manages, and troubleshoots any servers or the network for the project team.
  \item[Testers] are responsible for verifying that the system meets the customer's requirements, and that the system performs those functions correctly.
  \item[Deployers] are responsible for deploying new releases of the system to physical servers.
  \item[Technical Writers] write user documentation for the system.
\end{description}


\vfill
\nocite{*}  % add everything in bib file to bibliography, but don't actually cite
\bibliographystyle{plain}
\bibliography{refs}

\end{document}

